//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GuitarShopD
{
    using System;
    using System.Collections.Generic;
    
    public partial class Contact
    {
        public System.Guid id { get; set; }
        public Nullable<System.Guid> userId { get; set; }
        public Nullable<System.Guid> contactTypeId { get; set; }
        public string Value { get; set; }
    
        public virtual ContactType ContactType { get; set; }
        public virtual user user { get; set; }
    }
}
