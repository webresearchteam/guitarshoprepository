﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace GuitarShopD.Controllers
{
    
    public class HomeController : Controller
    {
       

        public ActionResult Index()
        {
            var context = new GSDEVEntities();
            List<Category> categories = new List<Category>();
            foreach (Category c in context.Categories)
            {
                if(c.ParentID == null)
                    categories.Add(c);
            }
            return View(categories);
        }
        public ActionResult CategoryDirect(string id)
        {
            var context = new GSDEVEntities();
            List<Category> categories = new List<Category>();
            foreach (Category c in context.Categories)
            {
                if (c.ParentID.ToString() == id)
                    categories.Add(c);
            }
            return View("CategoryDirect", categories);
        }

        public ActionResult SubCategoryDirect(string id)
        {
            var context = new GSDEVEntities();
            List<Purpose> purpose = new List<Purpose>();
            foreach (Purpose p in context.Purposes)
            {

                if (p.Item.CategoryID.ToString() == id)
                {
                    purpose.Add(p);
                    ViewBag.console = "" + p.Item.Category.Name.ToString();
                }
            }
            return View("SubCategoryDirect", purpose);
        }
    }
}