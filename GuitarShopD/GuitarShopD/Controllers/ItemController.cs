﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GuitarShopD.Controllers
{
    public class ItemController : Controller
    {
        // GET: Item
        public ActionResult GetItem(string id)
        {
            var context = new GSDEVEntities();
            var purposeList = context.purposePrices;
            var currentPurpose = from current in purposeList
                                 where current.Purpose.id.ToString() == id
                                 select current;
            return View(currentPurpose);
        }
    }
}